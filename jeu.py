import pygame, sys

pygame.init()

WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 250, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
CYAN = (0, 255, 255)
PURPLE = (255, 0, 255)

window = pygame.display.set_mode((800, 800)) # creation de la fenetre de 800px par 800px
font = pygame.font.SysFont("arial", 16)

rectangle = (300, 300, 100, 100) # (abscisse, ordonnée, largeur, hauteur)
rectangle_color = WHITE

def display_text(text, x, y, color):
    label = font.render(text, True, color)
    window.blit(label, (x, y))

while True:
    pygame.Surface.fill(window, 0) # effacer l'ecran
    for event in pygame.event.get():
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
            pygame.quit()
            sys.exit(0)
        if event.type == pygame.KEYDOWN and event.key == pygame.K_a:
            rectangle_color = BLUE # si on appuie sur 'a', on modifie la couleur du rectangle à 'bleu'

    display_text("Bonjour à tous", 100, 100, BLUE) # on affiche ce texte en position 100, 100 en bleu
    pygame.draw.rect(window, rectangle_color, rectangle) # on affiche en blanc un rectangle
    pygame.draw.circle(window, GREEN, (100, 200), 50)
    pygame.display.update()
